iEngage Labs
Brewery Order Service Docs

GET BEER

One showing how to make a request using cURL:

include::{snippets}/v2/beer-get/curl-request.adoc[]

One showing the HTTP request:

include::{snippets}/v2/beer-get/http-request.adoc[]

And one showing the HTTP response:

include::{snippets}/v2/beer-get/http-response.adoc[]

Response Body:
include::{snippets}/v2/beer-get/response-body.adoc[]

Response Fields:
include::{snippets}/v2/beer-get/response-fields.adoc[]

NEW BEER

One showing how to make a request using cURL:

include::{snippets}/v2/beer-save/curl-request.adoc[]

One showing the HTTP request:

include::{snippets}/v2/beer-save/http-request.adoc[]

And one showing the HTTP response:

include::{snippets}/v2/beer-save/http-response.adoc[]

Response Body:
include::{snippets}/v2/beer-save/response-body.adoc[]

Request Fields
include::{snippets}/v2/beer-save/request-fields.adoc[]

Request Fields
include::{snippets}/v2/beer-save/request-fields.adoc
