package com.iengagelabs.beerworks.web.model.v2;

public enum BeerStyleEnum {
    LAGER, PILSNER, PALE_ALE, STOUT, GOSE, PORTER, WHEAT, IPA, SAISON
}
