package com.iengagelabs.beerworks.web.mappers;

import com.iengagelabs.beerworks.domain.Customer;
import com.iengagelabs.beerworks.web.model.v1.CustomerDtoV1;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerMapper {

    CustomerDtoV1 customerToCustomerV1Dto(Customer customer);

    Customer customerDtoV1toCustomer(CustomerDtoV1 customerDtoV1);
}
