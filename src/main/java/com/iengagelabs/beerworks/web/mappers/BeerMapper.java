package com.iengagelabs.beerworks.web.mappers;

import com.iengagelabs.beerworks.domain.Beer;
import com.iengagelabs.beerworks.web.model.v2.BeerDtoV2;
import org.mapstruct.Mapper;

@Mapper(uses = {DateMapper.class})
public interface BeerMapper {
    BeerDtoV2 beerToBeerV2Dto(Beer beer);

    Beer beerDtoV2toBeer(BeerDtoV2 beerDtoV2);
}
