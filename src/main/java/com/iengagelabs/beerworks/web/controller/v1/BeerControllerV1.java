package com.iengagelabs.beerworks.web.controller.v1;

import com.iengagelabs.beerworks.services.v1.BeerServiceV1;
import com.iengagelabs.beerworks.web.model.v1.BeerDtoV1;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Deprecated
@RequestMapping(value = BeerControllerV1.API_V1_BEER, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
@RestController
public class BeerControllerV1 {

    public static final String API_V1_BEER = "/api/v1/beer";
    private final BeerServiceV1 beerServiceV1;

    public BeerControllerV1(BeerServiceV1 beerServiceV1) {
        this.beerServiceV1 = beerServiceV1;
    }

    @GetMapping({"/{beerId}"})
    public ResponseEntity<BeerDtoV1> getBeer(@PathVariable("beerId") UUID beerId) {
        return new ResponseEntity<>(beerServiceV1.getBeerById(beerId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity handlePost(@RequestBody BeerDtoV1 beerDtoV1) {
        BeerDtoV1 savedBeerDtoV1 = beerServiceV1.saveNewBeer(beerDtoV1);

        HttpHeaders headers =  new HttpHeaders();
        headers.add("Location", API_V1_BEER + savedBeerDtoV1.getId().toString());
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @PutMapping({"/{beerId}"})
    public ResponseEntity handleUpdate(@PathVariable("beerId") UUID beerId, @Valid @RequestBody BeerDtoV1 beerDto){
        beerServiceV1.updateBeer(beerId, beerDto);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping({"/{beerId}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBeer(@PathVariable("beerId") UUID beerId) {
        beerServiceV1.deleteBeerById(beerId);
    }

}
