package com.iengagelabs.beerworks.web.controller.v1;

import com.iengagelabs.beerworks.services.v1.CustomerServiceV1;
import com.iengagelabs.beerworks.web.model.v1.CustomerDtoV1;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Validated
@RequestMapping(CustomerControllerV1.API_V1_CUSTOMER)
@RestController
public class CustomerControllerV1 {

    public static final String API_V1_CUSTOMER = "/api/v1/customer";
    private final CustomerServiceV1 customerServiceV1;

    public CustomerControllerV1(CustomerServiceV1 customerServiceV1) {
        this.customerServiceV1 = customerServiceV1;
    }

    @GetMapping("/{customerId}")
    public ResponseEntity<CustomerDtoV1> getCusotmer(@PathVariable("customerId") UUID customerId) {
        return new ResponseEntity<>(customerServiceV1.getCustomerById(customerId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity handlePost(@Valid @RequestBody CustomerDtoV1 customerDtoV1) {
        CustomerDtoV1 savedCustomerDtoV1 = customerServiceV1.saveNewCustomer(customerDtoV1);

        HttpHeaders headers =  new HttpHeaders();
        headers.add("Location", API_V1_CUSTOMER + savedCustomerDtoV1.getId().toString());
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @PutMapping({"/{customerId}"})
    public ResponseEntity handleUpdate(@PathVariable("customerId") UUID customerId, @Valid @RequestBody CustomerDtoV1 customerDtoV1){
        customerServiceV1.updateCustomer(customerId, customerDtoV1);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping({"/{customerId}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCustomer(@PathVariable("customerId") UUID customerId) {
        customerServiceV1.deleteCustomerById(customerId);
    }

}
