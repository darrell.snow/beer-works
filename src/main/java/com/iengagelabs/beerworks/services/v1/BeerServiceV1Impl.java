package com.iengagelabs.beerworks.services.v1;

import com.iengagelabs.beerworks.web.model.v1.BeerDtoV1;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class BeerServiceV1Impl implements BeerServiceV1 {

    @Override
    public BeerDtoV1 getBeerById(UUID beerId) {
        return BeerDtoV1.builder().id(UUID.randomUUID())
                .beerName("Galaxy Cat")
                .beerStyle("Pale Ale")
                .build();
    }

    @Override
    public BeerDtoV1 saveNewBeer(BeerDtoV1 beerDtoV1) {
        return BeerDtoV1.builder()
                .id(UUID.randomUUID())
                .build();
    }

    @Override
    public BeerDtoV1 updateBeer(UUID beerId, BeerDtoV1 beerDtoV1) {
        return BeerDtoV1.builder()
                .id(UUID.randomUUID())
                .build();
    }
    @Override
    public void deleteBeerById(UUID beerId) {
        log.debug("Delete beerId: " + beerId);
    }


}
