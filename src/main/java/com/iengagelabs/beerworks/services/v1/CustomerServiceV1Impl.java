package com.iengagelabs.beerworks.services.v1;

import com.iengagelabs.beerworks.web.model.v1.CustomerDtoV1;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;
@Slf4j
@Service
public class CustomerServiceV1Impl implements CustomerServiceV1 {

    @Override
    public CustomerDtoV1 getCustomerById(UUID customerId) {
        return CustomerDtoV1.builder().id(UUID.randomUUID())
                .customerName("Lion Pub")
                .build();
    }

    @Override
    public CustomerDtoV1 saveNewCustomer(CustomerDtoV1 customerDtoV1) {
        return CustomerDtoV1.builder()
                .id(UUID.randomUUID())
                .build();
    }

    @Override
    public CustomerDtoV1 updateCustomer(UUID customerId, CustomerDtoV1 customerDtoV1) {
        return CustomerDtoV1.builder()
                .id(UUID.randomUUID())
                .build();
    }
    @Override
    public void deleteCustomerById(UUID customerId) {
        log.debug("Delete customerId: " + customerId);
    }


}
