package com.iengagelabs.beerworks.services.v1;

import com.iengagelabs.beerworks.web.model.v1.CustomerDtoV1;

import java.util.UUID;

public interface CustomerServiceV1 {
    CustomerDtoV1 getCustomerById(UUID customerId);

    CustomerDtoV1 saveNewCustomer(CustomerDtoV1 customerDtoV1);

    CustomerDtoV1 updateCustomer(UUID customerId, CustomerDtoV1 customerDtoV1);

    void deleteCustomerById(UUID customerId);
}
