package com.iengagelabs.beerworks.web.model.v2;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;

@ActiveProfiles("kebab")
@JsonTest
public class BeerDtoV2KebabTest extends BaseBeerDtoV2Test {

    @Test
    void testSerializeBeerDtoV2Kebab() throws JsonProcessingException {
        BeerDtoV2 beerDto = getDto();

        String jsonString = objectMapper.writeValueAsString(beerDto);

        System.out.println(jsonString);
    }

    @Test
    void testDeserializeDtoKebabCase() throws IOException {

        String json = "{\"id\":\"21d3ff46-9219-42af-9b64-999e4248d44c\",\"beer-name\":\"Galaxy Cat\",\"beer-style\":\"PALE_ALE\",\"version\":null,\"created-date\":\"2020-01-05T06:57:11.088021-08:00\",\"last-modified-date\":\"2020-01-05T06:57:11.089539-08:00\",\"price\":14.95,\"quantity-on-hand\":null,\"upc\":123456789}\n";

        BeerDtoV2 beerDto = objectMapper.readValue(json, BeerDtoV2.class);

        System.out.println(beerDto);
    }
}