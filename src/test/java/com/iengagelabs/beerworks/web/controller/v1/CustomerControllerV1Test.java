package com.iengagelabs.beerworks.web.controller.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iengagelabs.beerworks.services.v1.CustomerServiceV1;
import com.iengagelabs.beerworks.web.model.v1.CustomerDtoV1;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CustomerControllerV1.class)
class CustomerControllerV1Test {

    @MockBean
    CustomerServiceV1 customerServiceV1;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    CustomerDtoV1 validCustomer;

    @BeforeEach
    void setUp() {
        validCustomer = CustomerDtoV1.builder()
                .id(UUID.randomUUID())
                .customerName("Lion Pub")
                .build();
    }

    @Test
    void getCustomer() throws Exception {
        given(customerServiceV1.getCustomerById(any(UUID.class))).willReturn(validCustomer);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/customer/" + validCustomer.getId().toString())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(validCustomer.getId().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.customerName").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.customerName").value(validCustomer.getCustomerName()));
    }

    @Test
    void handlePost() throws Exception {
        CustomerDtoV1 customerDtoV1 = validCustomer;
        customerDtoV1.setId(null);
        CustomerDtoV1 savedDto = CustomerDtoV1.builder().id(UUID.randomUUID()).customerName("Tiger Pub").build();
        String customerDtoJson = objectMapper.writeValueAsString(customerDtoV1);

        given(customerServiceV1.saveNewCustomer(any())).willReturn(savedDto);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/v1/customer/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(customerDtoJson))
                .andExpect(status().isCreated());

    }

    @Test
    void handleUpdate() throws Exception {
        CustomerDtoV1 customerDtoV1 = validCustomer;
        String customerDtoJson = objectMapper.writeValueAsString(customerDtoV1);
        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/v1/customer/" + validCustomer.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(customerDtoJson))
                .andExpect(status().isNoContent());

        then(customerServiceV1).should().updateCustomer(any(), any());
    }

    @Test
    void handleDelete() throws Exception {
        CustomerDtoV1 customerDtoV1 = validCustomer;

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/v1/customer/" + validCustomer.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

    }
}