package com.iengagelabs.beerworks.web.controller.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iengagelabs.beerworks.services.v1.BeerServiceV1;
import com.iengagelabs.beerworks.web.model.v1.BeerDtoV1;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BeerControllerV1.class)
class BeerControllerV1Test {

    public static final String API_V1_BEER = "/api/v1/beer/";
    @MockBean
    BeerServiceV1 beerServiceV1;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    BeerDtoV1 validBeer;

    @BeforeEach
    void setUp() {
        validBeer = BeerDtoV1.builder().id(UUID.randomUUID())
                .beerName("Galaxy Cat")
                .beerStyle("PALE_ALE")
                .upc(123456789L)
                .build();
    }

    @Test
    void getBeer() throws Exception {
        given(beerServiceV1.getBeerById(any(UUID.class))).willReturn(validBeer);

        mockMvc.perform(MockMvcRequestBuilders
                .get(API_V1_BEER + validBeer.getId().toString())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(validBeer.getId().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.beerName").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.beerName").value(validBeer.getBeerName()));
    }

    @Test
    void handlePost() throws Exception {
        BeerDtoV1 beerDtoV1 = validBeer;
        beerDtoV1.setId(null);
        BeerDtoV1 savedDto = BeerDtoV1.builder().id(UUID.randomUUID()).beerName("Solar Kitty").build();
        String beerDtoJson = objectMapper.writeValueAsString(beerDtoV1);

        given(beerServiceV1.saveNewBeer(any())).willReturn(savedDto);

        mockMvc.perform(MockMvcRequestBuilders
                .post(API_V1_BEER)
                .contentType(MediaType.APPLICATION_JSON)
                .content(beerDtoJson))
                .andExpect(status().isCreated());

    }

    @Test
    void handleUpdate() throws Exception {
        BeerDtoV1 beerDto = validBeer;
        beerDto.setId(null);
        String beerDtoJson = objectMapper.writeValueAsString(beerDto);

        mockMvc.perform(MockMvcRequestBuilders
                .put(API_V1_BEER + UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON)
                .content(beerDtoJson))
                .andExpect(status().isNoContent());

        then(beerServiceV1).should().updateBeer(any(), any());
    }

    @Test
    void handleDelete() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                .delete(API_V1_BEER + validBeer.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

    }
}